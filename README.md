# ETP schema only Java source generation 

Example of generating and validating and packaging Java classes and jars from an ETP avro protocol specification ( `.avpr` ).

## Requirements

-  JDK 8.
-  Maven 3.0 or later.

## Compilation and usage

### `mvn package`

Generates Java class sources from the avro ETP protocol schema located in the `src/main/resources/avro/etp.avpr` direcotry,
then validates these generated classes against the same schema.

If successful, the generated Java classe are packaged into a `jar` in `target/etp-java-schema-only-1.2-SNAPSHOT.jar` which whould typically 
get published to a public artifact repository such as [Maven Central](https://search.maven.org/).

### `mvn install`

Same as `mvn package`, but adds copying the artifacts to local `~/.m2` cache for use with dependent software. 

## **TODO**

- Add source jar manifest information including changeset hash.
